import argparse
import os
import phonenumbers
import pandas as pd
import math
import numpy as np
import time

start_time = time.time()

parser = argparse.ArgumentParser(
    description="Cleans Phone numbers according to their country codes. Requires xlsx file with 'Country' and 'Phone' headers"
)
parser.add_argument(
    "file",
    type=str,
    help="file name (including full path)",
)

args = parser.parse_args()

file_name = os.path.basename(args.file)

input_data = pd.read_excel(args.file)
all_coountry_details = pd.read_excel("./AllCountryCodes.xlsx")
known_countries = all_coountry_details["COUNTRY"].tolist()
known_isd_codes = all_coountry_details["COUNTRY CODE"].tolist()
known_iso2_codes = all_coountry_details["ISO"].tolist()


data = {
    "Country": input_data["Country"],
    "ISO": "",
    "Phone": "",
    "Country Status": "",
    "Phone Status": "",
}

output_data = pd.DataFrame(data=data)

input_data = input_data.replace(np.nan, "", regex=True)


def add_country_code(phone, country_code, row_index):

    phone_header = "Phone"
    phone_status_header = "Phone Status"

    try:
        ph_num = phonenumbers.parse(phone, country_code)
        output_data.at[row_index, phone_header] = phonenumbers.format_number(ph_num, phonenumbers.PhoneNumberFormat.E164)
        output_data.at[row_index, phone_status_header] = "Modified"
    except Exception as e:
        output_data.at[row_index, phone_header] = phone
        output_data.at[row_index, phone_status_header] = "Invalid"
        print(str(row_index) + ": add_country_code: " + str(e))


for index, row in input_data.iterrows():

    iso_country = None
    if row["Country"]:
        # get ISO code and put it in the row
        c_index = known_countries.index(row["Country"].strip())
        iso_country = known_iso2_codes[c_index]
        output_data.at[index, "ISO"] = iso_country
    else:
        output_data.at[index, "Country Status"] = "Absent"

    phone1 = str(row["Phone"]).replace(" ", "")
    if phone1:
        try:
            # phone1 = str(int(phone1))

            ph_num = phonenumbers.parse("+" + phone1, None)
            # Verify if the guessed code is in eu range
            # if yes
            if ph_num.country_code in known_isd_codes:
                output_data.at[index, "Phone"] = phonenumbers.format_number(ph_num, phonenumbers.PhoneNumberFormat.E164)
                # Check if the code matches the given country
                country_from_number = (
                    phonenumbers.phonenumberutil.region_code_for_country_code(
                        ph_num.country_code
                    )
                )
                if iso_country:
                    if iso_country != country_from_number:
                        # If not mark it as Mismatch
                        output_data.at[index, "Country Status"] = "Mismatch"
                else:
                    # Guess the country
                    guess_index = known_isd_codes.index(ph_num.country_code)
                    output_data.at[index, "Country"] = known_countries[guess_index]
                    output_data.at[index, "Country Status"] = "Guessed"

            # if not
            else:
                # Probably country code needs to be added
                # if country details are provided
                if iso_country:
                    # Add country code and mark as Modified
                    add_country_code(phone1, iso_country, index)
                # if not
                else:
                    # give it back
                    output_data.at[index, "Phone"] = phone1
                    # mark as Invalid
                    output_data.at[index, "Country Status"] = "Absent"
                    output_data.at[index, "Phone Status"] = "Invalid"

        except phonenumbers.phonenumberutil.NumberParseException as npe:
            # This might also mean the country code is not present in the given number
            # if country details are provided
            # Add country code and mark as Modified
            if iso_country:
                add_country_code(phone1, iso_country, index)
            else:
                # give it back
                output_data.at[index, "Phone"] = phone1
                # mark as Invalid
                output_data.at[index, "Country Status"] = "Absent"
                output_data.at[index, "Phone Status"] = "Invalid"
        except Exception as e:
            # give it back
            output_data.at[index, "Phone"] = phone1
            output_data.at[index, "Phone Status"] = "Invalid"
            print(str(index) + ": Phone1: " + str(e))


# pylint: disable=abstract-class-instantiated
writer = pd.ExcelWriter("output/clean_" + file_name, engine="xlsxwriter")
output_data.to_excel(writer, sheet_name="clean_data")
workbook = writer.book
worksheet = writer.sheets["clean_data"]
len_df = len(output_data.index) + 1


writer.save()