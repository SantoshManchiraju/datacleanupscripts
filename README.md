
# Data CleanUp #

This contains scripts for cleaning data in spreadsheets.

The scripts are not smart enough to understand the content of the spreadsheets. For any changes in the headers of spreadsheet, the scripts should be updated accrodingly.

### Supported Formats ###
* xlsx
* ods

### Pre-requisites ###
* Python3 (preferably python3.7 or above) installed
* Git

*Note: If you're using Linux, you're blessed.* :wink: *By default you get Python3 and git with the distro. If you're using you can downlad python from [here](https://www.python.org/downloads/) and git from [here](https://git-scm.com/download/win). Am not sure how it is on mac. Please contact me if this is needed, we can set it up together and I can document the steps here.*

### How do I get set up? ###

#### Setup for Linux/MacOS ####

* Create Virtual environment
  ```
  $ python3 -m venv env
  ```
* Enter Vitual environment
  ```
  $ source env/bin/activate
  ```
* Install Dependencies
  ```
  $ pip install -r requirements.txt
  ```

#### Setup for Windows ####

* Create Virtual environment
  ```
  $ py -m venv env
  ```
* Enter Vitual environment
  ```
  $ .\env\Scripts\activate
  ```
* Install Dependencies
  ```
  $ pip install -r requirements.txt
  ```

You can leave the virtual environment by using `deactivate` command.

### How do I use the scripts? ###
Once you enter the virtual environment, you can execute any of the scripts in the directory like this:
  ```
  python <script> <input file path>
  ```
  
example:
  ```
  python clean_phonenumbers.py /home/krishna/Downloads/mydata.xlsx
  ```
  
Cleaned data is stored as a sperate file inside output directory.

### Contribution guidelines ###

* Always make a branch from `develop` for any contribution. Do not touch `master`
* Make Pull Requests to `develop` branch
* Other guidelines -> To be updated

### Who do I talk to? ###

* Me :) (Krishna)