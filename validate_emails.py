import argparse
import pandas as pd
import re
import math

from email_validator import validate_email, EmailNotValidError


def is_first_name_invalid(name: str) -> bool:
    if not name:
        return True
    return not bool(re.fullmatch("[A-zÀ-ž-.' ]{2,}", name))


def is_last_name_invalid(name: str) -> bool:
    if not name:
        return True
    return not bool(re.fullmatch("[A-zÀ-ž-.' ]{1,}", name))


def printProgressBar(
    iteration,
    total,
    prefix="Progress:",
    suffix="Complete",
    decimals=1,
    length=50,
    fill="█",
    printEnd="\r",
):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + "-" * (length - filledLength)
    print(f"\r{prefix} |{bar}| {percent}% {suffix}", end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


def just_validate(data):
    invalid_count = 0
    tot_recs = data.shape[0]

    for index, row in data.iterrows():
        is_record_invalid = False

        try:
            # Validate.
            validate_email(str(row["email"]))
        except EmailNotValidError as e:
            is_record_invalid = True
            print(str(index + 2) + ": Email: " + str(e))

        # validate first name
        first_name = row["first_name"]
        if is_first_name_invalid(first_name):
            is_record_invalid = True
            print(str(index + 2) + ": First Name: " + first_name + " not valid")

        # validate last name
        last_name = row["last_name"]
        if is_last_name_invalid(last_name):
            is_record_invalid = True
            print(str(index + 2) + ": Last Name: " + last_name + " not valid")

        if is_record_invalid:
            invalid_count += 1
            print("-")
        # printProgressBar(index, tot_recs)

    print("\n\nInvalid records: " + str(invalid_count))


def validate_and_correct(data):
    invalid_count = 0
    corrected_count = 0
    tot_recs = data.shape[0]

    for index, row in data.iterrows():
        is_record_invalid = False
        is_record_corrected = False

        try:
            # Validate.
            valid = validate_email(str(row["email"]))
            # Update with the normalized form.
            data.at[index, "email"] = valid.email
        except EmailNotValidError as e:
            data.at[index, "email"] = str(row["email"])
            is_record_invalid = True
            print(str(index + 2) + ": Email: " + str(e))

        # validate first name
        first_name = row["first_name"]
        if is_first_name_invalid(first_name):
            is_record_invalid = True
            is_record_corrected = True
            data.at[index, "first_name"] = "-"
            print(
                str(index + 2)
                + ": First Name: "
                + first_name
                + " not valid, replaced with -"
            )
        else:
            data.at[index, "first_name"] = first_name

        # validate last name
        last_name = row["last_name"]
        if is_last_name_invalid(last_name):
            is_record_invalid = True
            is_record_corrected = True
            data.at[index, "last_name"] = "-"
            print(
                str(index + 2)
                + ": Last Name: "
                + last_name
                + " not valid, replaced with -"
            )
        else:
            data.at[index, "last_name"] = last_name

        if is_record_invalid:
            invalid_count += 1
            # print("-")

        if is_record_corrected:
            corrected_count += 1
        printProgressBar(index, tot_recs)

    print("\n\nInvalid records: " + str(invalid_count))
    print("Corrected records: " + str(corrected_count))

    return data


def main():
    parser = argparse.ArgumentParser(description="Validates names and email addresses")
    parser.add_argument(
        "file",
        type=str,
        help="file name (including full path)",
    )

    parser.add_argument(
        "-a",
        "--auto_correct",
        action="store_true",
        help="Select this option to auto correct the data.\nNote: It corrects only first and last names. Email needs manual intervention",
    )

    args = parser.parse_args()
    data = None

    data = pd.read_excel(
        args.file, converters={"first_name": str, "last_name": str, "email": str}
    )
    data = data.replace(math.nan, "")
    # data["first_name"] = str(data["first_name"]).strip()
    # data["last_name"] = str(data["last_name"]).strip()

    if args.auto_correct:
        corrected_data = validate_and_correct(data)

        # pylint: disable=abstract-class-instantiated
        writer = pd.ExcelWriter("output/clean_email_data.xlsx", engine="xlsxwriter")
        corrected_data.to_excel(writer, sheet_name="clean_data")
        writer.save()
        print("Clean data saved as output/clean_email_data.xlsx")
    else:
        just_validate(data)


if __name__ == "__main__":
    main()